package com.epic.abcc.action.login;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by geeth_g on 10/10/2018.
 */
@Controller
@RequestMapping("")
public class LoginController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView helloWorld() {
        String message = "Login Page";
        return new ModelAndView("login/login", "message", message);
    }
}
